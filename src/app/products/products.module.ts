import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {RouterModule, ActivatedRoute, Router, Routes} from "@angular/router";
import { MainComponent } from "./main/main.component";
import { ProductsComponent } from "./products.component";
import { ProductComponent } from "./product/product.component";

export const routes: Routes = [
    {path: "", redirectTo: "main", pathMatch: "full"},
    { path: 'main', component: MainComponent },
    {path: ":id", component: ProductComponent}
];

@NgModule({
    declarations: [
        ProductsComponent,
        ProductComponent,
        MainComponent
    ],
    exports: [
        ProductsComponent,
        ProductComponent,
        MainComponent
    ],

    imports: [
        CommonModule,
        RouterModule
    ]
})
export class ProductsModule {}