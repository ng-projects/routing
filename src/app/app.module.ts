import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {
	RouterModule,
	Routes
 } from '@angular/router';

import { AppComponent } from './app.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ProtectedComponent } from './protected/protected.component';
import { ProductsComponent } from './products/products.component';
import { LocationStrategy, HashLocationStrategy, APP_BASE_HREF } from '@angular/common';
import { MainComponent } from './products/main/main.component';
import { ProductComponent } from './products/product/product.component';

import {routes as childRoutes, ProductsModule} from './products/products.module';
import { AUTH_PROVIDERS } from './auth.service';
import { LoggedInGuard } from './logged-in.guard';


const routes: Routes = [
	{path: '', redirectTo: 'home', pathMatch: 'full'},
	{path: 'home', component: HomeComponent},
	{path: 'about', component: AboutComponent},
	{path: 'contact', component: ContactComponent},
	{path: 'contactus', redirectTo: 'contact'},

	//authentication demo
	{path: 'login', component: LoginComponent},
	{
		path: 'protected',
		component: ProtectedComponent,
		canActivate: [LoggedInGuard]
	},

	//nested
	{
		path: 'products',
		component: ProductsComponent,
		children: childRoutes
	},
];
@NgModule({
	declarations: [
	  AppComponent,
	  ContactComponent,
	  AboutComponent,
	  HomeComponent,
	  LoginComponent,
	  ProtectedComponent,

	  ProductsComponent,
	  MainComponent,
	  ProductComponent,

	],
	imports: [
		  BrowserModule,
		//   FormsModule,
		//   HttpModule,
		  // routes
		  RouterModule.forRoot(routes)
	],
	providers: [
		AUTH_PROVIDERS,
		LoggedInGuard
		//hash roting
		// {provide: LocationStrategy, useClass: HashLocationStrategy},
		//base tag programatically
		// {provide: APP_BASE_HREF, useValue: '/'}
	],
	bootstrap: [AppComponent]
  })
export class AppModule { }
